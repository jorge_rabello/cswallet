package br.concrete.onboarding.wallet.service;

import br.concrete.onboarding.wallet.entity.Wallet;
import br.concrete.onboarding.wallet.entity.WalletStatus;
import br.concrete.onboarding.wallet.errors.exceptions.WalletNotFoundException;
import br.concrete.onboarding.wallet.repository.WalletRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class WalletServiceTest {

    @Mock
    private WalletRepository repository;

    @InjectMocks
    private WalletService service;

    @Test
    @DisplayName("When try to retrieve all saved wallets then should get a list of saved wallets")
    public void WhenTryToRetrieveAllSavedWalletsThenShouldGetAListOfSavedWallets() {
        List<Wallet> savedWalletsMock = Arrays.asList(
                new Wallet(
                        "01ARZ3NDEKTSV4RRFFQ69G5FAV",
                        "USD Wallet",
                        "USD",
                        new BigDecimal("350.75"),
                        LocalDateTime.now(),
                        LocalDateTime.now(),
                        WalletStatus.ACTIVE
                ),
                new Wallet(
                        "01EKM05797JHDG08K80AP50MQR",
                        "EUR Wallet",
                        "EUR",
                        BigDecimal.ZERO,
                        LocalDateTime.now(),
                        LocalDateTime.now(),
                        WalletStatus.INACTIVE
                ),
                new Wallet(
                        "01EKM05YJ2EX9V1VQ58ETH0WGV",
                        "JPY Wallet",
                        "JPY",
                        new BigDecimal("50.0"),
                        LocalDateTime.now(),
                        LocalDateTime.now(),
                        WalletStatus.ACTIVE
                )
        );

        when(repository.findAll()).thenReturn(savedWalletsMock);
        List<Wallet> savedWallets = service.findAll();
        assertNotNull(savedWallets);
        assertFalse(savedWallets.isEmpty());
        assertEquals(3, savedWallets.size());
        verify(repository, times(1)).findAll();
    }

    @Test
    @DisplayName("Given a wallet id when try to retrieve a wallet by id then should get a wallet")
    public void GivenAWalletIdWhenTryToRetrieveAWalletByIdThenShouldGetAWallet() {

        Wallet savedWallet = new Wallet(
                "01ARZ3NDEKTSV4RRFFQ69G5FAV",
                "USD Wallet",
                "USD",
                new BigDecimal("350.75"),
                LocalDateTime.now(),
                LocalDateTime.now(),
                WalletStatus.ACTIVE
        );

        Optional<Wallet> optionalWallet = Optional.of(savedWallet);

        when(repository.findById("01ARZ3NDEKTSV4RRFFQ69G5FAV")).thenReturn(optionalWallet);

        Wallet walletMock = new Wallet(
                "01ARZ3NDEKTSV4RRFFQ69G5FAV",
                "USD Wallet",
                "USD",
                new BigDecimal("350.75"),
                LocalDateTime.now(),
                LocalDateTime.now(),
                WalletStatus.ACTIVE
        );

        Wallet wallet = service.findById("01ARZ3NDEKTSV4RRFFQ69G5FAV");

        assertNotNull(wallet);
        assertEquals("USD", wallet.getCurrency());
        verify(repository, times(1)).findById(anyString());
    }

    @Test
    @DisplayName("Given a wallet id when try to retrieve a wallet by id and can't find it then should throws a WalletNotFoundException")
    public void GivenAWalletIdWhenTryToRetrieveAWalletByIdAndCantFindItThenShouldThrowsAWalletNotFoundException() {

        Wallet savedWallet = new Wallet(
                "01ARZ3NDEKTSV4RRFFQ69G5FAV",
                "USD Wallet",
                "USD",
                new BigDecimal("350.75"),
                LocalDateTime.now(),
                LocalDateTime.now(),
                WalletStatus.ACTIVE
        );

        Optional<Wallet> optionalWallet = Optional.of(savedWallet);

        when(repository.findById("01ARZ3NDEKTSV4RRFFQ69G5FAV")).thenReturn(Optional.empty());

        assertThrows(WalletNotFoundException.class, () -> service.findById("01ARZ3NDEKTSV4RRFFQ69G5FAV"));

        verify(repository, times(1)).findById(anyString());
    }

    @Test
    @DisplayName("Given a wallet id when try to retrieve a wallet by id and gets a null return then should throws a WalletNotFoundException")
    public void GivenAWalletIdWhenTryToRetrieveAWalletByIdAndGetsANullReturnThenShouldThrowsAWalletNotFoundException() {

        when(repository.findById("01ARZ3NDEKTSV4RRFFQ69G5FAV")).thenReturn(null);

        assertThrows(WalletNotFoundException.class, () -> service.findById("01ARZ3NDEKTSV4RRFFQ69G5FAV"));

        verify(repository, times(1)).findById(anyString());
    }
}
