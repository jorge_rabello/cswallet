package br.concrete.onboarding.wallet.endpoints;

import br.concrete.onboarding.wallet.entity.Wallet;
import br.concrete.onboarding.wallet.input.InputWallet;
import br.concrete.onboarding.wallet.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/wallets")
public class WalletEndpoint {

    private final WalletService service;

    @Autowired
    public WalletEndpoint(WalletService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody InputWallet inputWallet) {
        Wallet savedWallet = service.save(inputWallet);
        return new ResponseEntity(savedWallet, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<?> findAll() {
        List<Wallet> savedWallets = service.findAll();
        return new ResponseEntity(savedWallets, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") String id) {
        Wallet savedWallet = service.findById(id);
        return new ResponseEntity(savedWallet, HttpStatus.OK);
    }
}
