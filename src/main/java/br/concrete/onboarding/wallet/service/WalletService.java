package br.concrete.onboarding.wallet.service;

import br.concrete.onboarding.wallet.entity.Wallet;
import br.concrete.onboarding.wallet.entity.WalletStatus;
import br.concrete.onboarding.wallet.errors.exceptions.WalletNotFoundException;
import br.concrete.onboarding.wallet.input.InputWallet;
import br.concrete.onboarding.wallet.repository.WalletRepository;
import io.azam.ulidj.ULID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class WalletService {

    private final WalletRepository repository;

    @Autowired
    public WalletService(WalletRepository repository) {
        this.repository = repository;
    }

    public Wallet save(InputWallet inputWallet) {

        Wallet wallet = new Wallet(
                ULID.random(),
                inputWallet.getAlias(),
                inputWallet.getCurrency(),
                BigDecimal.ZERO,
                LocalDateTime.now(),
                LocalDateTime.now(),
                WalletStatus.INACTIVE
        );
        Wallet savedWallet = repository.save(wallet);
        return savedWallet;
    }

    public List<Wallet> findAll() {
        return repository.findAll();
    }

    public Wallet findById(String id) {
        Optional<Wallet> optionalWallet = repository.findById(id);
        if (optionalWallet == null) {
            throw new WalletNotFoundException("Wallet not found for id: " + id);
        }
        return optionalWallet.orElseThrow(() -> new WalletNotFoundException("Wallet not found for id: " + id));
    }
}
