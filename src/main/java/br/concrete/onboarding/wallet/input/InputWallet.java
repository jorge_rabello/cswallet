package br.concrete.onboarding.wallet.input;

import lombok.*;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class InputWallet {

    private String alias;
    private String currency;

}
