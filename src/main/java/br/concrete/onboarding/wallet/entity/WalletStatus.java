package br.concrete.onboarding.wallet.entity;

public enum WalletStatus {

    ACTIVE("active"), INACTIVE("inactive");

    private final String value;

    WalletStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
