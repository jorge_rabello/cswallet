package br.concrete.onboarding.wallet.entity;

import io.azam.ulidj.ULID;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Document("wallet")
public class Wallet implements Serializable {

    @Id
    @Field("_id")
    private String id;
    private String alias;
    private String currency;
    private BigDecimal balance;
    private LocalDateTime createdDate;
    private LocalDateTime changedDate;
    private WalletStatus status;

}
