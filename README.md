# Wallet Onboarding Project

## Pre requisites

### Docker
> * Windows 
>   * https://docs.docker.com/docker-for-windows/install/
> * Mac 
>   * https://docs.docker.com/docker-for-mac/install/
> * Linux 
>   * https://docs.docker.com/engine/install/ubuntu/
>
> NOTES: 
> 
> * If you're using Linux, execute the post install
>     * https://docs.docker.com/engine/install/linux-postinstall/
> 
> * No matter what your S.O. rebooot after install

### Docker Compose

> * Multi S.O.
>     * https://docs.docker.com/compose/install/

### MongoDB Compass

> * Multi S.O.
>     * https://docs.mongodb.com/compass/master/install/

### Postman
> * Multi S.O.
>   * https://www.postman.com/downloads/

## Build
> ```
> $ ./gradlew clean build
> ```

## Swagger
> Copy ```/wallet/docs/swagger/swagger.yaml``` content into https://editor.swagger.io/

## Docker MongoDB Container

### Start MongoDB Docker Container

> ```
> $ docker-compose up -d
> ```

### Check MongoDB Docker Container
> ```
>  $ docker-compose ps
>      Name                  Command             State            Ports          
> -------------------------------------------------------------------------------
> wallet_mongo_1   docker-entrypoint.sh mongod   Up      0.0.0.0:27017->27017/tcp
> ```

## Stop MongoDB Docker Container
> ```
> $ docker-compose down
> ```